import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit, AfterViewInit{

  constructor() { }

  ngOnInit() {
    let curr2 = document.getElementById('personal');
    curr2.style.display = "block";
    document.getElementById('tab1').classList.add("active");
  }
  ngAfterViewInit() {
    setTimeout(() => {
      var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems,{
          dist: 0,
          padding: 0,
          fullWidth: true,
          indicators: true,
          duration: 100,
    });
    }, 500);
  }
  open(tabId, cityName) {
    // Declare all variables
    let i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.querySelectorAll(".tabcontent");
    console.log(tabcontent)
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    let curr = document.getElementById(cityName);
    curr.style.display = "block";
    document.getElementById(tabId).className += " active";
  }
}
