import { Component, OnInit } from '@angular/core';
import { NavcompoService } from '../navcompo.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  compo: string;
  constructor(private navigate: NavcompoService) { }

  ngOnInit() {
    if (this.navigate.navname === '') {
      this.compo = 'Recommended';
    } else {
    this.compo = this.navigate.navname;
  }
  document.addEventListener('DOMContentLoaded', function() {
    const elems = document.querySelectorAll('.sidenav');
    const instances = M.Sidenav.init(elems, Option);
  });
  }
}
