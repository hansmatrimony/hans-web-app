import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit, AfterViewInit{
  fileContent: any;
  contents: any;
  reader: FileReader;
  fileExtensionMessage: string;
  fileExtensionError: boolean;
  fileExtension: string;
  allowedExtensions: string[];
  fileName: string;
  imgUrl: any;
  file: File;
  constructor() {
   }
  ngOnInit() {
    let curr2 = document.getElementById('personal');
    curr2.style.display = "block";
    document.getElementById('tab1').classList.add("active");
  }
  ngAfterViewInit() {
    setTimeout(function(){
      var elems = document.querySelector('.datepicker');
      var instances = this.M.Datepicker.init(elems, Option);
    }, 500);
    setTimeout(function(){
      var elems2 = document.querySelector('.timepicker');
      var instances2 = this.M.Timepicker.init(elems2, Option);
    }, 500);
  }
  fileEvent(event) {
    this.file = <File>event.target.files[0];
    console.log(this.file);
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.imgUrl = e.target.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    this.fileName = this.file.name;
    this.allowedExtensions = ['jpg', 'png', 'jpeg', 'gif'];
    this.fileExtension = this.fileName.split('.').pop();

    if (this.isInArray(this.allowedExtensions, this.fileExtension)) {
      this.fileExtensionError = false;
      this.fileExtensionMessage = '';
    } else {
      this.fileExtensionMessage = 'Only jpg and png are allowed!!';
      this.fileExtensionError = true;
    }
    if (this.file) {
      this.reader = new FileReader();
      this.reader.onloadend = (e: any) => {
        this.contents = e.target.result;
        this.fileContent = this.contents;
      };
      this.reader.readAsDataURL(this.file);
    } else {
      alert('Failed to load file');
    }
  }
  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1;
  }
  open(tabId, cityName) {
    // Declare all variables
    let i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.querySelectorAll(".tabcontent");
    console.log(tabcontent)
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    let curr = document.getElementById(cityName);
    curr.style.display = "block";
    document.getElementById(tabId).className += " active";
    if(tabId == 'tab1'){
      setTimeout(function(){
        var elems = document.querySelector('.datepicker');
        var instances = this.M.Datepicker.init(elems, Option);
      }, 500);
      setTimeout(function(){
        var elems2 = document.querySelector('.timepicker');
        var instances2 = this.M.Timepicker.init(elems2, Option);
      }, 500);
    }
}
}
