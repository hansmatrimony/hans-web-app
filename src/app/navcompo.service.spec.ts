import { TestBed, inject } from '@angular/core/testing';

import { NavcompoService } from './navcompo.service';

describe('NavcompoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavcompoService]
    });
  });

  it('should be created', inject([NavcompoService], (service: NavcompoService) => {
    expect(service).toBeTruthy();
  }));
});
