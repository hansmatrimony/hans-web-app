import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import * as $ from 'jquery';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LikedMeComponent } from './liked-me/liked-me.component';
import { ConnectionComponent } from './connection/connection.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { NavcompoService } from './navcompo.service';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    EditProfileComponent,
    ViewProfileComponent,
    FooterComponent,
    NavbarComponent,
    LikedMeComponent,
    ConnectionComponent,
    RecommendedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [NavcompoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
