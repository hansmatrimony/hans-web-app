import {style} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  highlightedDiv: number;
  fileContent: any;
  contents: any;
  reader: FileReader;
  fileExtensionMessage: string;
  fileExtensionError: boolean;
  fileExtension: string;
  allowedExtensions: string[];
  fileName: string;
  imgUrl: any;
  file: File;
  constructor() {

  }
  M: any;
  currentTab = 0;
  ngOnInit() {

    this.showTab(this.currentTab);
  }
  validateForm() {
    // This function deals with validation of the form fields
    let x, z, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[this.currentTab].getElementsByTagName("input");
    z = x[this.currentTab].getElementsByClassName("must");
    console.log(z);
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
      // If a field is empty...
      if(this.currentTab == 10 ) {
        if(i == 4){
         continue;
        }
        else {
        if (y[i].value == "") {
          // add an "invalid" class to the field:
          y[i].className += " invalid";
          (<HTMLElement>document.querySelector('.error')).style.display = 'block';
          // and set the current valid status to false:
          valid = false;
        }
      }
      }
      else if (y[i].value == "") {
        // add an "invalid" class to the field:
        y[i].className += " invalid";
        (<HTMLElement>document.querySelector('.error')).style.display = 'block';
        // and set the current valid status to false:
        valid = false;
      }

    }
       for( i = 0; i < z.length; i++) {
       if(z[i].selectedIndex == 0){
         z[i].className += " invalid";
         (<HTMLElement>document.querySelector('.error')).style.display = 'block';
         valid = false;
       }
     }
    if(this.currentTab == 10){
      document.getElementById('email_err').style.display = 'none';
      document.getElementById('mob_err').style.display = 'none';
      document.getElementById('whatsapp_err').style.display = 'none';
      document.getElementById('adhar_err').style.display = 'none';
      y[0].className = "";
      y[2].className = "";
      y[3].className = "";
      y[4].className = "";
        var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
        if(String(y[0].value).search (filter) == -1){
          valid = false;
          document.getElementById('email_err').style.display = 'block';
          y[0].className = " invalid";
        }
        if(String(y[2].value).search (/(7|8|9)\d{9}/) == -1){
          valid = false;
          document.getElementById('mob_err').style.display = 'block';
          y[2].className = " invalid";
        }
        if(String(y[3].value).search (/(7|8|9)\d{9}/) == -1){
          valid = false;
          document.getElementById('whatsapp_err').style.display = 'block';
          y[3].className = " invalid";
        }
        if(y[4].value !== ""){
        if(String(y[4].value).search (/^\d{4}\s\d{4}\s\d{4}$/) == -1){
          valid = false;
          document.getElementById('adhar_err').style.display = 'block';
          y[4].className = " invalid";
        }
      }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
      (<HTMLElement>document.querySelector('.error')).style.display = 'none';
      document.getElementsByClassName("step")[this.currentTab].className += " finish";
    }
    return valid; // return the valid status
  }
  showTab(n) {
    // This function will display the specified tab of the form ...
    let x = document.getElementsByClassName("tab");
    (<HTMLElement>x[n]).style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
      (<HTMLElement>document.querySelector(".prevBtn")).style.display = "none";
      (<HTMLElement>document.querySelector(".nextBtn")).style.left = "50vw";
      (<HTMLElement>document.querySelector(".nextBtn")).style.transform = "translate(-50%)";
      (<HTMLElement>document.querySelector(".nextBtn")).style.top = "70vh";
      document.querySelector("#con-out").classList.remove("outer");
    } else {
      (<HTMLElement>document.querySelector(".nextBtn")).style.top = "85vh";
      (<HTMLElement>document.querySelector(".nextBtn")).style.left  = "75%";
      (<HTMLElement>document.querySelector(".prevBtn")).style.display = "inline";
      document.querySelector("#con-out").classList.add("outer");
    }
    if (n == (x.length - 1)) {
      (<HTMLElement>document.querySelector(".nextBtn")).style.display = "none";
      (<HTMLElement>document.querySelector(".prevBtn")).style.display = "none";

    } else {
      document.querySelector(".nextBtn").innerHTML = '<i class="material-icons">arrow_forward</i>';
    }
    if((n == 8) || (n == 9) || (n == 10) || (n == 11)){
      (<HTMLElement>document.querySelector(".nextBtn")).style.backgroundColor = "#085C95";
      (<HTMLElement>document.querySelector(".prevBtn")).style.backgroundColor = "#085C95";
    }else {
      (<HTMLElement>document.querySelector(".nextBtn")).style.backgroundColor = "#F00E67";
      (<HTMLElement>document.querySelector(".prevBtn")).style.backgroundColor = "#F00E67";
    }
    if(n == 1){
      setTimeout(function(){
        var elems = document.querySelector('.datepicker');
        var instances = this.M.Datepicker.init(elems, Option);
      }, 500);
    }
    if(n == 4){
      console.log(n);
      setTimeout(function(){
        var elems2 = document.querySelector('.timepicker');
        console.log(elems2);
        var instances2 = this.M.Timepicker.init(elems2, Option);
      }, 500);
    }
    // ... and run a function that displays the correct step indicator:
    this.fixStepIndicator(n)
  }
  nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !this.validateForm()) {return false; }
    // Hide the current tab:
    (<HTMLElement>x[this.currentTab]).style.display = "none";
    // Increase or decrease the current tab by 1:
    this.currentTab = this.currentTab + n;
    // if you have reached the end of the form... :
    if (this.currentTab >= x.length) {
      //...the form gets submitted:
      return false;
    }
    // Otherwise, display the correct tab:
    this.showTab(this.currentTab);
    console.log(this.currentTab);
  }
  fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    let i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
  }
  fileEvent(event) {
    document.getElementById('ProfilePic').style.display = "inline";
    this.file = <File>event.target.files[0];
    console.log(this.file);
    const reader = new FileReader();
    reader.onload = (e: any) => {
        this.imgUrl = e.target.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    this.fileName = this.file.name;
    this.allowedExtensions = ['jpg', 'png', 'jpeg', 'gif'];
    this.fileExtension = this.fileName.split('.').pop();

    if (this.isInArray(this.allowedExtensions, this.fileExtension)) {
      this.fileExtensionError = false;
      this.fileExtensionMessage = '';
    } else {
      this.fileExtensionMessage = 'Only jpg and png are allowed!!';
      this.fileExtensionError = true;
    }
    if (this.file) {
      this.reader = new FileReader();
      this.reader.onloadend = (e: any) => {
        this.contents = e.target.result;
        this.fileContent = this.contents;
      };
      this.reader.readAsDataURL(this.file);
    } else {
      alert('Failed to load file');
    }
  }
  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1;
  }
}
