import { Component, OnInit } from '@angular/core';
import { NavcompoService } from '../navcompo.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private navigate: NavcompoService) { }

  ngOnInit() {
  }
  navcomp(name) {
    this.navigate.navname = name;
  }
}
