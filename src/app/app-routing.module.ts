import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { ConnectionComponent } from './connection/connection.component';
import { LikedMeComponent } from './liked-me/liked-me.component';
import { RecommendedComponent } from './recommended/recommended.component';

const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'edit-profile', component: EditProfileComponent},
    {path: 'view-profile', component: ViewProfileComponent},
    {path: 'connection', component: ConnectionComponent},
    {path: 'like-me', component: LikedMeComponent},
    {path: 'recommended', component: RecommendedComponent },
    {path: '**', redirectTo: ''}
    ];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

}
