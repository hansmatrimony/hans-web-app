import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

 private _date = Date.now();
  public get date() {
    console.log(this._date);
    return this._date;
  }
  public set date(value) {
    this._date = value;
  }
  constructor() { }

  ngOnInit() {
    document.addEventListener('DOMContentLoaded', function() {
      // tslint:disable-next-line:prefer-const
      const elems = document.querySelectorAll('.sidenav');
      // tslint:disable-next-line:prefer-const
      const instances = M.Sidenav.init(elems, Option);
    });
  }
}
